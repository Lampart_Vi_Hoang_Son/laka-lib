<?php

namespace Laka\Lib\Test\Support;

use Laka\Lib\Entities\RoomEntity;
use Laka\Lib\Repositories\Room\RoomRepositoryOptimize;
use Laka\Lib\Support\Collection;
use Tests\TestCase;

class LakaCollectionTest extends TestCase {

    public function testWhere() {
        $rooms = RoomRepositoryOptimize::all();
        $collection = Collection::make($rooms);
        dump($collection->where('id',108));
        $this->markTestSkipped();
    }
    public function testWhereNot() {
        $rooms = RoomRepositoryOptimize::all();
        $collection = Collection::make($rooms);
        dump($collection->where('name','!=','name123')->toArray());
        $this->markTestSkipped();
    }

}
