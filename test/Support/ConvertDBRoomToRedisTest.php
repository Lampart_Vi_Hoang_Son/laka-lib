<?php

namespace Laka\Lib\Test\Support;

use App\Models\RoomModel;
use App\User;
use Laka\Lib\Repositories\Room\RoomRepositoryOptimize;
use Laka\Lib\Services\ConvertDBRoomToRedis;
use Tests\TestCase;

class ConvertDBRoomToRedisTest extends TestCase {

    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    public function testConverRoom() {
        return;
        /** @var ConvertDBRoomToRedis $convert_DB_room_to_redis */
        $convert_DB_room_to_redis = $this->app->make(ConvertDBRoomToRedis::class);
        $idAllRoom                = $convert_DB_room_to_redis->start();
        $this->assertTrue(true);
    }

    public function testgetRoomMemberByRoom() {
        $room = RoomRepositoryOptimize::find(3936);
        dd(RoomRepositoryOptimize::getRoomMemberByRoom($room));
    }

    public function testallRoom() {

        (RoomRepositoryOptimize::all());

        $this->assertTrue(true);
    }

    public function testallByUser() {
        $all_user = User::all();
        foreach ($all_user as $user) {
            dump($user->name . ": " . RoomRepositoryOptimize::allByUser($user)
                                                            ->count()) . PHP_EOL;
        }
        $this->assertTrue(true);
    }

    public function testFindRoom() {
        $all = RoomModel::all();
        foreach ($all as $room) {
            dump(RoomRepositoryOptimize::find($room->id));
        }
    }

}
