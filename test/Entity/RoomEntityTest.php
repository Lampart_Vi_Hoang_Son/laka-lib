<?php

namespace Laka\Lib\Test\Entity;

use Laka\Lib\Entities\RoomEntity;
use Tests\TestCase;

class RoomEntityTest extends TestCase {

    /**
     * @param $data
     *
     * @dataProvider dataCreate
     */
    public function testCreate($data,$expected) {
        $room = new RoomEntity($data);
        $room->save();
        $this->assertEquals($expected['name'],$room->getName());
        $this->assertNotNull($room->getName());
        $this->assertEquals($expected['description'],$room->getDescription());
        $this->assertEquals($expected['icon_img'],$room->getIconImg());
        $this->assertEquals($expected['can_add_user'],$room->getCanAddUser());
    }

    public function dataCreate() {
        return [
            [
                [
                    'name' => 'name123',
                    'id'   => null,
                    'description'=> 'description',
                    'icon_img'=> 'icon',
                    'can_add_user'=> 1,
                    'is_my_chat'=> 0,
                    'created'=> time(),
                    'updated'=> time(),
                    'pin'=> time(),
                ],
                [
                    'name' => 'name123',
                    'id'   => null,
                    'description'=> 'description',
                    'icon_img'=> 'icon',
                    'can_add_user'=> 1,
                    'is_my_chat'=> 0,
                    'created'=> time(),
                    'updated'=> time(),
                    'pin'=> time(),
                ],
            ],
            [
                [
                    'name' => 'name12323123',
                    'id'   => 1
                ],
                [
                    'name' => 'name12323123',
                    'id'   => null,
                    'description'=> null,
                    'icon_img'=> null,
                    'can_add_user'=> null,
                    'is_my_chat'=> null,
                    'created'=> null,
                    'updated'=> null,
                    'pin'=> null,
                ],
            ]
        ];
    }

}
