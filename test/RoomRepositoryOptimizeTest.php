<?php

namespace Laka\Lib\Repositories\Room;

use Illuminate\Contracts\Auth\Factory;
use Illuminate\Support\Collection;

use Illuminate\Support\Facades\Redis;
use Laka\Lib\Entities\RoomEntity;
use Laka\Lib\Entities\RoomMemberEntity;
use Laka\Lib\Factories\EntityFactory;
use Laka\Lib\Factories\RoomFactory;
use Laka\Lib\Factories\RoomMemberFactory;
use Tests\TestCase;

class RoomRepositoryOptimizeTest extends TestCase {

    public function testGetAll() {
        $this->assertInstanceOf(\Laka\Lib\Support\Collection::class,RoomRepositoryOptimize::all());
    }

    public function testCreateCanAddUser() {
        $room = new RoomEntity(['can_add_user'=>1,'name'=>'room 1']);
        //$room->setCanAddUser(0);
        $room->save();
        $this->assertInstanceOf(RoomEntity::class,$room);
    }


    public function testRedis(){
        $this->markTestSkipped();
        Redis::set('m',2);
        echo Redis::get('m');
    }

    public function testSaveEntity() {
        $e = new RoomEntity();
        $e->save();
        $this->assertIsInt($e->id);
    }
    public function testRoomEntity() {
        try{
            $room = new RoomEntity(['name'=>"sss",'can_add_user'=>1]);
            $room->save();
            $r = RoomRepositoryOptimize::find($room->getId());
            $room_member = new RoomMemberEntity(['room_id'=>$room->id,'user_id'=>90,'role_in_room'=>1]);
            $room_member->save();

            $this->assertIsInt($room_member->getId());

            // $room = RoomRepositoryOptimize::find(1);
            $collectionRoomMember = RoomRepositoryOptimize::getRoomMemberByRoom($room);
            //dump($collectionRoomMember->where('id',1));
            dump($collectionRoomMember->first()->id);
            dump($collectionRoomMember->where('is_my_chat','',0)->where('id','',2));
        }catch (\Exception $e){
            echo $e->getMessage();
        }

        //        dd($room);
        //        dump($room);

        //        dd($collectionRoomMember);
    }

    public function testName() {
        $this->markTestSkipped();
        try{
            $room = RoomRepositoryOptimize::find(1);
            $collectionRoomMember = RoomRepositoryOptimize::getRoomMemberByRoom($room);
            dump($collectionRoomMember->where('id',1));
            dump($collectionRoomMember->first()->id);
            dump($collectionRoomMember->where('is_my_chat','',0)->where('id','',2));
        }catch (\Exception $e){
            echo $e->getMessage();
        }

//        dd($room);
//        dump($room);

//        dd($collectionRoomMember);
    }

    public function testAll() {
        dump(RoomRepositoryOptimize::all()->toJson());
        $this->assertIsString(RoomRepositoryOptimize::all()->toJson());
    }
}
