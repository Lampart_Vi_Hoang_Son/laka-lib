<?php

namespace Laka\Lib\Test\Repository\Group;

use App\User;
use Illuminate\Support\Facades\Auth;
use Laka\Lib\Entities\BaseEntity;
use Laka\Lib\Entities\GroupEntity;
use Laka\Lib\Entities\GroupItemEntity;

use Laka\Lib\Entities\RoomEntity;
use Laka\Lib\Repositories\Group\GroupItemRepositoryOptimize;

use Laka\Lib\Repositories\Group\GroupRepositoryOptimize;
use Laka\Lib\Repositories\Room\RoomRepositoryOptimize;
use Laka\Lib\Support\Collection;
use Tests\TestCase;

class GroupRepositoryTest extends TestCase {

    protected function setUp(): void {
        parent::setUp();

        // User
        $this->actingAs(User::find(90));
        $roomall = RoomRepositoryOptimize::allByUser(Auth::user());

        // Lấy danh sách room
        $arrayRoomId = ($roomall->map(function ($room) {
            return $room->getId();
        })
                                ->toArray());

        $data = [
            'id'        => null,
            'room_list' => $arrayRoomId,
            'created'   => time(),
            'name'      => 'GroupName_' . time(),
            'updated'   => time(),
            'status'    => 1,
        ];
        $rs   = new GroupEntity($data);
        $rs->save();

    }

    public function testGroupItem(){
        $group = (GroupRepositoryOptimize::allByUser(User::find(7))->first());
        dd(GroupItemRepositoryOptimize::allByGroup($group));
    }

    public function testAllByUser() {
        $users = User::all();
        foreach ($users as $user){
            $all = GroupRepositoryOptimize::allByUser($user);
            if($all->count()>0){
                dump($all);
            }
        }
        $this->assertInstanceOf(Collection::class, $all);
    }

    public function testAllByGroup() {
        $all = GroupItemRepositoryOptimize::allByGroup(GroupRepositoryOptimize::all()
                                                      ->first());
        $this->assertInstanceOf(Collection::class, $all);
    }

    public function testAll() {

        $group = new GroupEntity();
        //$group->setId();
        $group->setName('test name');
        $group->setCreated(time());
        $group->setUpdated(time());
        $group->setStatus(1);
        $group->setUserId(90);
        $group->save();
        $all = GroupRepositoryOptimize::all();

        $groupItem = new GroupItemEntity();
        //$group->setId();
        $groupItem->setGroupId($group->getId());
        $groupItem->setCreated(time());
        $groupItem->setUpdated(time());
        /** @var RoomEntity $room */
        $room1 = new RoomEntity(['name' => 'room_name']);
        $room1->save();
        $room = RoomRepositoryOptimize::all()
                                      ->first();
        $groupItem->setRoomId($room->getId());
        $groupItem->save();
        $all = GroupItemRepositoryOptimize::all();
        //dd($all);
        $rs = GroupRepositoryOptimize::all();
        $this->assertInstanceOf(Collection::class, $rs);
    }

    public function testFind() {
        $rs = GroupRepositoryOptimize::find(1);
        $this->assertInstanceOf(GroupEntity::class, $rs);
    }

}
