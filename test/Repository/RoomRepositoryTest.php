<?php

namespace Laka\Lib\Test\Entity;

use App\User;
use Laka\Lib\Entities\RoomEntity;
use Laka\Lib\Entities\RoomMemberEntity;
use Laka\Lib\Repositories\Room\RoomRepositoryOptimize;
use Laka\Lib\Support\Collection;
use Tests\TestCase;

class RoomRepositoryTest extends TestCase {

    public function testAllByUser() {
        $user = User::all()->first();
        $rooms = RoomRepositoryOptimize::allByUser($user);
        dump($rooms);
        $this->assertInstanceOf(Collection::class, $rooms);
    }

    /**
     * @param $data
     *
     *
     */
    public function testAll() {
        $rooms = RoomRepositoryOptimize::all() ;
        $this->assertInstanceOf(Collection::class, $rooms);
    }

    public function testFind() {
        $rooms = RoomRepositoryOptimize::all();
        $id    = $rooms[0]->getId();
        $room  = RoomRepositoryOptimize::find($id);
        $this->assertInstanceOf(RoomEntity::class, $room);
    }

    public function testGetRoomMemberByRoom() {
        $room = new RoomEntity();
        $room->save() ;
        $rooms = RoomRepositoryOptimize::all();
        $id    = $rooms[0]->getId();
        $room  = RoomRepositoryOptimize::find($id);
        $room_member = new RoomMemberEntity(['user_id'=>90,'room_id'=>$room->getId()]);
        $room_member->save();
        $room_member = RoomRepositoryOptimize::getRoomMemberByRoom($room);
        $this->assertInstanceOf(Collection::class,$room_member);

    }

}
