<?php

namespace Laka\Lib\Test;

use Laka\Lib\Services\LakaDeploy;
use Tests\TestCase;

class LakaDeployTest extends TestCase {


    public function testDeploy() {
        $this->markTestSkipped();
        return;
        $this->markTestSkipped();
        dump(LakaDeploy::deploy('api', 'development', 'v1.15-dev'));
        $this->assertIsArray(LakaDeploy::deploy('frontend', 'production', 'v1.14'));
        $this->assertIsArray(LakaDeploy::deploy('backend', 'production', 'v1.14'));
        $this->assertIsArray(LakaDeploy::deploy('api', 'production', 'v1.10'));
    }

    public function testKey() {
        $this->markTestSkipped();
        return;
        $this->markTestSkipped();
        $response = (LakaDeploy::getVersion('api', 'development'));
        dump($response);
        $this->assertArrayHasKey("development", $response);
        $this->assertObjectHasAttribute("return", $response['development']);
        $this->assertObjectHasAttribute("server", $response['development']);
        $this->assertArrayHasKey("0", $response['development']->return);
    }

    public function testGetVersion() {
        $this->markTestSkipped();
        return;
        dump(LakaDeploy::deploy('api', 'development', 'v1.15-dev'));
        dump(LakaDeploy::getVersion('api', 'development'));
    }

    public function testGetVersionTmp() {
        $this->markTestSkipped();
        return;
        dump(LakaDeploy::getVersion('api', 'development'));
        $this->assertIsArray(LakaDeploy::getVersion('api', 'development'));
        $this->assertIsArray(LakaDeploy::getVersion('api', 'staging'));
        $this->assertIsArray(LakaDeploy::getVersion('api', 'production'));
        $this->assertIsArray(LakaDeploy::getVersion('backend', 'development'));
        $this->assertIsArray(LakaDeploy::getVersion('backend', 'staging'));
        $this->assertIsArray(LakaDeploy::getVersion('backend', 'production'));
        $this->assertIsArray(LakaDeploy::getVersion('frontend', 'development'));
        $this->assertIsArray(LakaDeploy::getVersion('frontend', 'staging'));
        $this->assertIsArray(LakaDeploy::getVersion('frontend', 'production'));
        $this->assertIsArray(LakaDeploy::getVersion('socket', 'development'));
        $this->assertIsArray(LakaDeploy::getVersion('socket', 'staging'));
        $this->assertIsArray(LakaDeploy::getVersion('socket', 'production'));
    }
}
