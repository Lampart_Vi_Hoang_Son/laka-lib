<?php

namespace Laka\Lib\Repositories\Room;

use Laka\Lib\Entities\GroupEntity;
use Laka\Lib\Entities\GroupItemEntity;
use Laka\Lib\Repositories\Group\GroupItemRepositoryOptimize;
use Laka\Lib\Repositories\Group\GroupRepositoryOptimize;
use Tests\TestCase;

class GroupRepositoryOptimizeTest extends TestCase {

    public function testGroupEntity() {
        $this->markTestSkipped();
        $g = new GroupEntity;
        dump($g);
    }

    public function testGroupFind() {
        $this->markTestSkipped();
        $g = GroupRepositoryOptimize::find(1);
        dump($g);
    }

    public function testGroupAll() {
        $this->markTestSkipped();
        $g = GroupRepositoryOptimize::all();
        dump($g);
    }

    public function testGroupItemFind() {
        $this->markTestSkipped();
        $g = GroupItemRepositoryOptimize::find(1);
        dump($g);
    }

    public function testGroupItemAll() {
        $this->markTestSkipped();
        $g = GroupItemRepositoryOptimize::all();
        dump($g);
    }

    public function testGroupItemEntity() {
        $this->markTestSkipped();
        $g = new GroupItemEntity();
        dump($g);
    }

}
