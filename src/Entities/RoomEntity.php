<?php

namespace Laka\Lib\Entities;

use App\Models\RoomModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Laka\Lib\Entities\Traits\getSetPropertyRoomEntity;
use Laka\Lib\Repositories\Room\RoomMemberRepositoryOptimize;
use Laka\Lib\Repositories\Room\RoomRepositoryOptimize;

class RoomEntity extends BaseEntity {

    use getSetPropertyRoomEntity;
    const CAN_ADD_USER = 1;
    const CAN_NOT_ADD_USER = 0;
    const TYPE_ROOM_MYCHAT = 1;
    const TYPE_ROOM_PUBLIC = 2;
    const TYPE_ROOM_COMPANY = 3;
    const TYPE_ROOM_DIRECTROOM = 4;

    protected $entity_name = 'room';

    protected $id;

    protected $name;
    protected $description;
    protected $icon_img;
    protected $can_add_user;
    protected $is_my_chat;
    protected $created;
    protected $updated;
    protected $status;
    protected $type;
    protected $pin;

    public function delete() {
        Redis::srem('ROOM_LIST', $this->id);
        $roomMembers = RoomRepositoryOptimize::getRoomMemberByRoom($this);
        foreach ($roomMembers as $room_member) {
            $room_member->delete();
        }
        Redis::del('ROOMMEMBER_' . $this->id);

        foreach (RoomRepositoryOptimize::MAPPING_PROPERTY as $key => $property) {
            $p = $property['property'];
            Redis::del($key . '_' . $this->id);
        }
        Cache::forget('allByUser' . Auth::id());

        return $this;
    }

    public function save() {
        if ($this->id === null) {
            $id                = (int) Redis::get('id_' . $this->entity_name);
            $roomMariadb       = new RoomModel;
            $roomMariadb->name = 'create by redis';
            $roomMariadb->save();
            $id = $roomMariadb->id;
            //$id++;
            Redis::set('id_' . $this->entity_name, $id);
        } else {
            $id = $this->id;
        }

        $this->id = $id;
        Redis::set($this->entity_name . "__" . $id, $this->toJson());
        Redis::sadd('ROOM_LIST', $id);

        foreach (RoomRepositoryOptimize::MAPPING_PROPERTY as $key => $property) {
            $p = $property['property'];
            if (isset($this->$p)) {
                if ($property['type'] === 'int') {
                    $this->$p = (int) $this->$p;
                }
                Redis::set($key . '_' . $id, $this->$p);
            } else {
                Redis::set($key . '_' . $id, '');
            }
        }
        Cache::forget('allRoom');
        Cache::forget('getRoomMemberByRoom' . $id);

        return true;
    }
}
