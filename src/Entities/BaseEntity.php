<?php

namespace Laka\Lib\Entities;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

class BaseEntity implements \JsonSerializable {

    public function __construct(array $array=[]){
        $keyProperty = array_keys(get_object_vars($this));
        foreach($array as $key =>$value){
            if(in_array($key,$keyProperty)){
                $this->$key= $value;
            }
        }
    }


    public function __get($name) {
        return $this->$name;
    }

    public function setAttribute($key, $value) {
        $this->$key = $value;
    }

    public function __toString() {
        return json_encode(get_object_vars($this));
    }

    public function getAttribute($key) {
        return $this->$key;
    }

    public function toJson() {
        return json_encode(get_object_vars($this));
    }

    public function with($property) {
        $this->listWith[] = $property;
    }

    public function toArray() {
        return (get_object_vars($this));
    }

    /**
     * @return bool
     */
    public function update() {
        if(!$this->id) return false;
        $id = $this->id;
        $this->id = $id;
        Redis::set($this->entity_name . "__" . $id, $this->toJson());
        (Redis::get('id_' . get_class($this)));

        return true;
    }

    /**
     * @return bool
     */
    public function save() {
        if ($this->id === null) {
            $id = (int) Redis::get('id_' . $this->entity_name);
            $id++;
            Redis::set('id_' . $this->entity_name, $id);
        } else {
            $id = $this->id;
        }

        $this->id = $id;
        Redis::set($this->entity_name . "__" . $id, $this->toJson());
        (Redis::get('id_' . get_class($this)));

        return true;
    }

    public function delete() {
        Redis::del($this->entity_name . "__" . (int) $this->id);
        ($this->id = null);
        return $this;
    }

    public function jsonSerialize() {
        return get_object_vars($this);
    }

}
