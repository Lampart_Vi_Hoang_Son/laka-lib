<?php

namespace Laka\Lib\Entities;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Redis;
use Laka\Lib\Entities\Traits\getSetPropertyRoomEntity;
use Laka\Lib\Repositories\Group\GroupItemRepositoryOptimize;
use Laka\Lib\Repositories\Group\GroupRepositoryOptimize;

class GroupItemEntity extends BaseEntity {

    protected $entity_name = 'group_item';
    protected $id;
    protected $group_id;
    protected $room_id;
    protected $created;
    protected $updated;
    protected $status;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getListRoom() {
        return $this->list_room;
    }

    /**
     * @param mixed $list_room
     */
    public function setListRoom($list_room): void {
        $this->list_room = $list_room;
    }

    /**
     * @return mixed
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created): void {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated): void {
        $this->updated = $updated;
    }

    /**
     * @return mixed
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getGroupId() {
        return $this->group_id;
    }

    /**
     * @param mixed $group_id
     */
    public function setGroupId($group_id): void {
        $this->group_id = $group_id;
    }

    /**
     * @return mixed
     */
    public function getRoomId() {
        return $this->room_id;
    }

    /**
     * @param mixed $room_id
     */
    public function setRoomId($room_id): void {
        $this->room_id = $room_id;
    }

    public function delete() {
        Redis::srem('GROUPITEM_LIST_' . $this->group_id, $this->room_id);
        foreach (GroupItemRepositoryOptimize::MAPPING_PROPERTY as $key => $property) {
            Redis::del($key . '_' . $this->group_id . '_' . $this->room_id);
        }

        return $this;
    }

    public function save() {
        if ($this->id === null) {
            $id = (int) Redis::get('id_' . $this->entity_name);
            $id++;
            Redis::set('id_' . $this->entity_name, $id);
        } else {
            $id = $this->id;
        }

        $this->id = $id;
        Redis::set($this->entity_name . "__" . $id, $this->toJson());
        Redis::sadd('GROUPITEM_LIST_' . $this->group_id, $this->room_id);
        foreach (GroupItemRepositoryOptimize::MAPPING_PROPERTY as $key => $property) {
            $p = $property['property'];
            if (isset($this->$p)) {
                if ($property['type'] === 'int') {
                    $this->$p = (int) $this->$p;
                }
                Redis::set($key . '_' . $this->group_id . '_' . $this->room_id, $this->$p);
            } else {
                Redis::set($key . '_' . $this->group_id . '_' . $this->room_id, '');
            }
        }        
        return true;
    }
}
