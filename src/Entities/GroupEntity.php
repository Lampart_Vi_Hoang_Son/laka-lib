<?php

namespace Laka\Lib\Entities;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Laka\Lib\Entities\Traits\getSetPropertyRoomEntity;
use Laka\Lib\Repositories\Group\GroupRepositoryOptimize;
use Laka\Lib\Repositories\Room\RoomRepositoryOptimize;

class GroupEntity extends BaseEntity {

    protected $entity_name = 'group';
    protected $id;
    protected $name;
    protected $user_id;
    protected $created;
    protected $updated;
    protected $status;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created): void {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated): void {
        $this->updated = $updated;
    }

    /**
     * @return mixed
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void {
        $this->status = $status;
    }


    /**
     * @return mixed
     */
    public function getUserId() {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id): void {
        $this->user_id = $user_id;
    }

    public function delete() {
        Redis::srem('GROUP_LIST_' . $this->user_id, $this->id);
        foreach (GroupRepositoryOptimize::MAPPING_PROPERTY as $key => $property) {
            Redis::del($key . '_' . $this->user_id . '_' . $this->id);
        }

        Cache::forget('allGroup' .  $this->user_id);
        Cache::forget('allGroupItem' . $this->id);
        

        return $this;
    }

    public function save() {

        if ($this->id === null) {
            $id = (int) Redis::get('id_' . $this->entity_name);
            $id++;
            Redis::set('id_' . $this->entity_name, $id);
        } else {
            $id = $this->id;
        }


        $this->id = $id;
        Redis::set($this->entity_name . "__" . $id, $this->toJson());
        Redis::sadd('GROUP_LIST_' . $this->user_id, $id);
        foreach (GroupRepositoryOptimize::MAPPING_PROPERTY as $key => $property) {
            $p = $property['property'];
            if (isset($this->$p)) {
                if ($property['type'] === 'int') {
                    $this->$p = (int) $this->$p;
                }
                Redis::set($key . '_' . $this->user_id . '_' . $id, $this->$p);
            } else {
                Redis::set($key . '_' . $this->user_id . '_' . $id, '');
            }
        }
        Cache::forget('allGroupItem' . $id);        
        Cache::forget('allGroup' .  $this->user_id);
        return true;
    }
}
