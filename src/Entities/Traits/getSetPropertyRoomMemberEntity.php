<?php

namespace Laka\Lib\Entities\Traits;



trait getSetPropertyRoomMemberEntity {


    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId() {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId(int $user_id): void {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getRoleInRoom() {
        return $this->role_in_room;
    }

    /**
     * @param mixed $role_in_room
     */
    public function setRoleInRoom(int $role_in_room): void {
        $this->role_in_room = $role_in_room;
    }

    /**
     * @return mixed
     */
    public function getJoinAt() {
        return $this->join_at;
    }

    /**
     * @param mixed $join_at
     */
    public function setJoinAt($join_at): void {
        $this->join_at = $join_at;
    }

    /**
     * @return mixed
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created): void {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     */
     public function setUpdated($updated): void {
        $this->updated = $updated;
    }

    /**
     * @return mixed
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getRoomId() {
        return $this->room_id;
    }

    /**
     * @param mixed $room_id
     */
    public function setRoomId($room_id): void {
        $this->room_id = $room_id;
    }


}
