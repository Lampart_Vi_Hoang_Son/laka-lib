<?php

namespace Laka\Lib\Entities\Traits;



trait getSetPropertyRoomEntity {


    /**
     * @return mixed
     */
    public function getPin() {
        return $this->pin;
    }

    /**
     * @param mixed $pin
     */
    public function setPin(int $pin): void {
        $this->pin = $pin;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getIconImg() {
        return $this->icon_img;
    }

    /**
     * @param mixed $icon_img
     */
    public function setIconImg($icon_img): void {
        $this->icon_img = $icon_img;
    }

    /**
     * @return mixed
     */
    public function getCanAddUser() {
        return $this->can_add_user;
    }

    /**
     * @param mixed $can_add_user
     */
    public function setCanAddUser(int $can_add_user): void {
        $this->can_add_user = $can_add_user;
    }

    /**
     * @return mixed
     */
    public function getIsMyChat() {
        return $this->is_my_chat;
    }

    /**
     * @param mixed $is_my_chat
     */
    public function setIsMyChat(int $is_my_chat): void {
        $this->is_my_chat = $is_my_chat;
    }

    /**
     * @return mixed
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created): void {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated): void {
        $this->updated = $updated;
    }

    /**
     * @return mixed
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void {
        $this->status = $status;
    }



    /**
     * @return mixed
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void {
        $this->type = (int) $type;
    }

}
