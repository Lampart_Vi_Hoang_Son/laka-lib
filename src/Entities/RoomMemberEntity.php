<?php

namespace Laka\Lib\Entities;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Laka\Lib\Entities\Traits\getSetPropertyRoomMemberEntity;
use Laka\Lib\Repositories\Group\GroupItemRepositoryOptimize;
use Laka\Lib\Repositories\Room\RoomMemberRepositoryOptimize;
use Laka\Lib\Repositories\Room\RoomRepositoryOptimize;

class RoomMemberEntity extends BaseEntity {

    const PERMISSION_ADMIN = 1;
    const PERMISSION_MEMBER = 0;
    const PERMISSION_READONLY = 2;

    use getSetPropertyRoomMemberEntity;

    protected $entity_name = 'room_member';

    protected $id;
    protected $user_id;
    protected $room_id;
    protected $role_in_room;
    protected $join_at;
    protected $created;
    protected $updated;


    public function save() {
        $this->save_element();
        if (!$this->hasExists()) {
        } else {
            $this->update_element();
        }
    }

    public function update_element() {

    }

    public function delete() {
        Redis::srem('ROOMMEMBER_' . $this->room_id, $this->user_id);
        Redis::srem('ROOM_BYUSERID_' . $this->user_id, $this->room_id);

        foreach (RoomMemberRepositoryOptimize::MAPPING_PROPERTY as $key => $property) {
            Redis::del($key . '_' . $this->room_id . '_' . $this->user_id);
        }
        Cache::forget('dataRoomMember:' . $this->user_id.':'.$this->room_id);
        return $this;
    }

    public function save_element() {
        if ($this->id === null) {
            $id = (int) Redis::get('id_' . $this->entity_name);
            $id++;
            Redis::set('id_' . $this->entity_name, $id);
        } else {
            $id = $this->id;
        }

        $this->id = $id;
        Redis::set($this->entity_name . "__" . $id, $this->toJson());
        Redis::sadd('ROOMMEMBER_' . $this->room_id, $this->user_id);

        foreach (RoomMemberRepositoryOptimize::MAPPING_PROPERTY as $key => $property) {
            $p = $property['property'];
            if (isset($this->$p)) {
                if ($property['type'] === 'int') {
                    $this->$p = (int) $this->$p;
                }
                Redis::set($key . '_' . $this->room_id . '_' . $this->user_id, $this->$p);
            } else {
                Redis::set($key . '_' . $this->room_id . '_' . $this->user_id, '');
            }
        }

        Redis::SADD("ROOM_BYUSERID_".$this->user_id,$this->room_id);

        Cache::forget('dataRoomMember:' . $this->user_id.':'.$this->room_id);
        Cache::forget('allByUser' . $this->user_id);
        Cache::forget('allRoom');

        return true;
    }

    private function hasExists(): bool {
        return RoomRepositoryOptimize::UserExistsOnRoom($this);
    }
}
