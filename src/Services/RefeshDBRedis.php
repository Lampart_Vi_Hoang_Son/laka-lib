<?php

namespace Laka\Lib\Services;

// php artisan tinker
//(new Laka\Lib\Services\RefeshDBRedis)->start();
//(new Laka\Lib\Services\RefeshDBRedis)->addNewKey();

use Illuminate\Support\Facades\Redis;

class RefeshDBRedis {

    public function start() {
        $keys = [
            'ROOMMEMBER_*',
            'ROOM_LIST',
            'ROOM_NAME_*',
            'ROOM_DESCRIPTION_*',
            'ROOM_ICON_*',
            'ROOM_CANADDUSER_*',
            'ROOM_ISMYCHAT_*',
            'ROOM_CREATED_*',
            'ROOM_UPDATED_*',
            'ROOM_STATUS_*',
            'ROOM_BYUSERID_*',
        ];

        foreach ($keys as $key) {
            $keys = Redis::keys($key);
            foreach ($keys as $key) {
                Redis::del($key);
            }
        }
    }

    public function addNewKey() {
        $list = Redis::KEYS('ROOMMEMBER_*');
        foreach ($list as $room) {
            //$room
            if (preg_match('/ROOMMEMBER_(\d+)$/', $room, $match)) {
                $room_id   = $match[1];
                $list_user = Redis::SMEMBERS($room);
                foreach ($list_user as $user_id) {
                    Redis::SADD('ROOM_BYUSERID_' . $user_id, $room_id);
                }
            }

        }
    }
}

