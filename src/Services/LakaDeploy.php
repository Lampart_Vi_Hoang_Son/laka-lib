<?php

namespace Laka\Lib\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use phpseclib3\Net\SSH2;
use phpseclib3\Crypt\PublicKeyLoader;

class LakaDeploy {


    public function __construc() {

    }


    /**
     * @param $server
     * @param $environment
     *
     * @return array
     * @throws GuzzleException
     */
    public static function getVersion($server, $environment): ?array {
        try {
            $link   = self::getLink($environment);
            $return = null;
            foreach ($link['list_environment'][$server][$environment] as $link) {
                $data                 = self::getRequest($link . '/api/get-version/' . $server, $environment);
                $return[$environment] = (json_decode($data));
            }

            return $return;
        } catch (\Exception $e) {
            return [$e->getMessage()];
        }
    }

    /**
     * @param string $server
     * @param string $environment
     * @param string $version
     *
     * @return array
     * @throws GuzzleException
     */
    public static function deploy(string $server, string $environment, string $version): ?array {
        
        if($environment == 'staging'){
            $return =[];
            
            $key = PublicKeyLoader::load(file_get_contents(app_path('../../key_laka_stg.pem')));
            $ssh = new SSH2('172.16.3.36');
            if (!$ssh->login('root',  $key)) {
                dd('Đăng nhập SSH không thành công');
            }else{
                switch ($server) {
                    case 'api':
                        $content = ($ssh->exec('cd /var/www/vhosts/server_api && git fetch && git reset --hard '.$version));
                        break;
                    case 'backend':
                        $content = ($ssh->exec('cd /var/www/vhosts/client_backend && git fetch && git reset --hard '.$version));
                        break;                        
                    case 'frontend':
                        $content = ($ssh->exec('cd /var/www/vhosts/frontend && git fetch && git reset --hard '.$version));
                        break;                    
                    default:
                        // code...
                        break;
                }
                $return['staging'] =['status'=>true,'data'=>[$content]];
                
                return $return;
            }            
        }


        


        try {
            $return = null;
            $link   = self::getLink($environment);
            foreach ($link['list_environment'][$server][$environment] as $link) {

                /** @var string $url format https://xxx.xxx/api/do-deploy/(api|backend|frontend|socket) */
                $url                  = $link . '/api/do-deploy/' . $server;
                $data                 = self::doDeploy($url, $version, $environment);
                $return[$environment] = (($data));
            }

            return $return;
        } catch (\Exception $e) {
            return [$e->getMessage()];
        }
    }

    /**
     * @param $link
     * @param $version
     *
     * @return array
     * @throws GuzzleException
     */
    private static function doDeploy(string $link, string $version, string $environment): ?array {

        $client = new Client();

        $data = [
            'form_params' => ['tag' => $version],
            'headers'     => [
                'secret' => config('deploytool.secret.' . $environment, '5432112345'),
                'Accept' => 'application/json'
            ]
        ];
        $url  = $link;

        $response = $client->request('POST', $url, $data);

        return [
            'status' => true,
            'data'   => json_decode($response->getBody()
                                             ->getContents())
        ];
    }

    /**
     * @param $link
     *
     * @return string
     * @throws GuzzleException
     */
    private static function getRequest(string $link, string $environment) {
        $client = new Client();
        $url    = $link;

        $data     = [
            'headers' => [
                'secret' => config('deploytool.secret.' . $environment, '5432112345'),
                'Accept' => 'application/json'
            ]
        ];
        $response = $client->request('GET', $url, $data);

        return $response->getBody()
                        ->getContents();
    }

    /**
     * @return \string[][][][]
     */
    private static function getLink($environment) {
        return [
            'list_environment' => [
                'api'      => [
                    'development' => ['http://172.16.2.8:8000'],
                    'staging'     => ['http://172.16.3.36:8000'],
                    'production'  => [config('deploytool.domain.' . $environment, '')],
                ],
                'backend'  => [
                    'development' => ['http://172.16.2.8:8000'],
                    'staging'     => ['http://172.16.3.36:8000'],
                    'production'  => [config('deploytool.domain.' . $environment, '')],
                ],
                'frontend' => [
                    'development' => ['http://172.16.2.8:8000'],
                    'staging'     => ['http://172.16.3.36:8000'],
                    'production'  => [config('deploytool.domain.' . $environment, '')],
                ],
                'socket'   => [
                    'development' => ['http://172.16.2.8:8000'],
                    'staging'     => ['http://172.16.3.36:8000'],
                    'production'  => [config('deploytool.domain.' . $environment, '')],
                ],
            ]
        ];
    }
}
