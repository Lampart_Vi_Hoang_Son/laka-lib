<?php

namespace Laka\Lib\Support;

class Collection extends \Illuminate\Support\Collection {

    public function where($key, $operator = null, $value = null) {
        if ($operator === '!=') {
            return $this->filter(function ($v, $k) use ($key, $operator, $value) {
                if ($value === null) {
                    $value = $operator;
                }

                return !($v->$key === $value);
            });
        }

        return $this->filter(function ($v, $k) use ($key, $operator, $value) {
            if ($value === null) {
                $value = $operator;
            }

            return $v->$key === $value;
        });
    }
}
