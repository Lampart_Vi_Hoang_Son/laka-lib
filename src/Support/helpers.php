<?php
if (! function_exists('LakeCollect')) {
    /**
     * Create a collect2ion from the given value.
     *
     * @param  mixed  $value
     * @return \Laka\Lib\Support\Collection
     */
    function LakaCollect($value = null)
    {
        return new  Laka\Lib\Support\Collection($value);
    }
}
