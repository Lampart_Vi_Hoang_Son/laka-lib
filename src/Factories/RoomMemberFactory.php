<?php

namespace Laka\Lib\Factories;

use Laka\Lib\Entities\RoomMemberEntity;

class RoomMemberFactory extends EntityFactory {

    protected $type;

    public function __construct() {
        $this->type = RoomMemberEntity::class;
    }
}
