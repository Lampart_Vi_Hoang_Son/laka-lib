<?php

namespace Laka\Lib\Factories;

use Laka\Lib\Entities\RoomEntity;

class EntityFactory {

    public function make($data) {
        $this->data = $data;
        $object = new $this->type([]);
        foreach ($this->data as $key => $value){
            $object->setAttribute($key,$value);
        }
        return $object ;
    }
}
