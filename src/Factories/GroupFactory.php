<?php

namespace Laka\Lib\Factories;

use Laka\Lib\Entities\GroupEntity;

class GroupFactory extends EntityFactory {

    protected $type;

    public function __construct() {
        $this->type = GroupEntity::class;
    }
}
