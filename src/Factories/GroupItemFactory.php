<?php

namespace Laka\Lib\Factories;

use Laka\Lib\Entities\GroupItemEntity;

class GroupItemFactory extends EntityFactory {

    /**
     * GroupItemFactory constructor.
     */
    protected $type;

    public function __construct() {
        $this->type = GroupItemEntity::class;
    }
}
