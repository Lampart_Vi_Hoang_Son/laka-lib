<?php

namespace Laka\Lib\Factories;

use Laka\Lib\Entities\RoomEntity;

class RoomFactory extends EntityFactory {

    protected $type;

    public function __construct() {
        $this->type = RoomEntity::class;
    }
}
