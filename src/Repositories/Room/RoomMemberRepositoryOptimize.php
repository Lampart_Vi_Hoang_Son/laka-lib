<?php

namespace Laka\Lib\Repositories\Room;

use Faker\Factory;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Laka\Lib\Entities\BaseEntity;
use Laka\Lib\Entities\GroupEntity;
use Laka\Lib\Entities\RoomEntity;
use Laka\Lib\Entities\RoomMemberEntity;
use Laka\Lib\Factories\RoomFactory;
use Laka\Lib\Factories\RoomMemberFactory;

/**
 * Class RoomMemberRepositoryOptimize
 *
 * @package Laka\Lib\Repositories\Room
 */
class RoomMemberRepositoryOptimize {

    /**
     *
     */
    const entity_name      = 'room_member';
    const MAPPING_PROPERTY = [
        'ROOMMEMBER_ID'         => ['property' => 'id', 'type' => 'int'],
        'ROOMMEMBER_USERID'     => ['property' => 'user_id', 'type' => 'int'],
        'ROOMMEMBER_ROLEINROOM' => ['property' => 'role_in_room', 'type' => 'int'],
        'ROOMMEMBER_JOINEDAT'   => ['property' => 'joined_at', 'type' => 'string'],
        'ROOMMEMBER_CREATED'    => ['property' => 'created_at', 'type' => 'string'],
        'ROOMMEMBER_UPDATED'    => ['property' => 'updated_at', 'type' => 'string'],
    ];

    /**
     * @return \Laka\Lib\Support\Collection
     */
    public static function all(): \Laka\Lib\Support\Collection {
        return LakaCollect([]);
    }

    /**
     * @param $id
     *
     * @return RoomMemberEntity|null
     */
    public static function find($roomId, $id, $values = null): ?RoomMemberEntity {
//        return Cache::rememberForever('dataRoomMember:' . $id.':'.$roomId, function () use ($roomId, $id) {
        $arrayMapping = self::MAPPING_PROPERTY;

        $obj = [];

        if ($values === null) {
            $mgetKeys = [];
            foreach ($arrayMapping as $key => $property) {
                $mgetKeys[] = $key . '_' . $roomId . '_' . $id;
            }
            $values = Redis::mget($mgetKeys);
        }

        $index = 0;
        foreach ($arrayMapping as $key => $property) {

//                $obj[$property['property']] = Redis::get($key . '_' . $roomId . '_' . $id);
            $obj[$property['property']] = $values[$index];
            if (($property['type']) === 'int') {
                if($property['property'] === 'role_in_room' && $obj[$property['property']]== null ){
                    $obj[$property['property']] = -1;
                }else{
                    $obj[$property['property']] = (int) $obj[$property['property']];
                }
            }
            $index++;
        }
        $obj['room_id'] = (int) $roomId;
        $obj['user_id'] = (int) $id;

        return (new RoomMemberEntity($obj));
//        });

    }

    public static function getFindQuery($roomId, $id)
    {
        $arrayMapping = self::MAPPING_PROPERTY;

        $mgetKeys = [];
        foreach ($arrayMapping as $key => $property) {
            $mgetKeys[] = $key . '_' . $roomId . '_' . $id;
        }
        return $mgetKeys;
    }

}
