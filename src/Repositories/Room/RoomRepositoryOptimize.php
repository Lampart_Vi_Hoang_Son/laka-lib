<?php

namespace Laka\Lib\Repositories\Room;

use App\User;
use Faker\Factory;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Laka\Lib\Entities\BaseEntity;
use Laka\Lib\Entities\GroupEntity;
use Laka\Lib\Entities\RoomEntity;
use Laka\Lib\Entities\RoomMemberEntity;
use Laka\Lib\Factories\RoomFactory;
use Laka\Lib\Factories\RoomMemberFactory;

/**
 * Class RoomRepositoryOptimize
 *
 * @package Laka\Lib\Repositories\Room
 */
class RoomRepositoryOptimize {

    const MAPPING_PROPERTY = [
        'ROOM_NAME'        => ['property' => 'name', 'type' => 'string'],
        'ROOM_DESCRIPTION' => ['property' => 'description', 'type' => 'string'],
        'ROOM_ICON'        => ['property' => 'icon_img', 'type' => 'string'],
        'ROOM_CANADDUSER'  => ['property' => 'can_add_user', 'type' => 'int'],
        'ROOM_ISMYCHAT'    => ['property' => 'is_my_chat', 'type' => 'int'],
        'ROOM_CREATED'     => ['property' => 'created', 'type' => 'int'],
        'ROOM_UPDATED'     => ['property' => 'updated', 'type' => 'int'],
        'ROOM_STATUS'      => ['property' => 'status', 'type' => 'string'],
        'ROOM_TYPE'        => ['property' => 'type', 'type' => 'int'],
    ];
    /**
     *
     */
    const entity_name = 'room';

    /**
     * @return \Laka\Lib\Support\Collection
     */
    public static function all(): \Laka\Lib\Support\Collection {
        $listRoomId = Redis::SMEMBERS('ROOM_LIST');
        $rs         = [];
        foreach ($listRoomId as $room_id) {
            $rs[] = RoomRepositoryOptimize::find($room_id);
        }

        return LakaCollect($rs);
    }

    /**
     * @return \Laka\Lib\Support\Collection
     */
    public static function allByUser(User $user): \Laka\Lib\Support\Collection {
        $listRoomId = Redis::SMEMBERS('ROOM_BYUSERID_' . $user->id);

        $queries = [];
        foreach ($listRoomId as $id) {
            $queries[] = self::getFindQuery((int) $id);
        }
        $values = Redis::pipeline(function ($pipe) use ($queries) {
            foreach ($queries as $query) {
                $pipe->mget($query);
            }
        });

//        $values = array_chunk($values, count(self::MAPPING_PROPERTY));

        $rs         = [];
        foreach ($listRoomId as $index => $id) {
            $rs[] = RoomRepositoryOptimize::find($id, $values[$index]);
        }

        return LakaCollect($rs);
    }

    /**
     * @param $id
     *
     * @return RoomEntity|null
     */
    public static function find($id, $values = null): ?RoomEntity {
        $arrayMapping = self::MAPPING_PROPERTY;

        if ($values === null) {
            foreach ($arrayMapping as $key => $property) {
                $keys[] = $key . '_' . $id;
            }
            $values = Redis::mget($keys);
        }

        $obj = [];
        $index = 0;
        foreach ($arrayMapping as $key => $property) {

            $obj[$property['property']] = $values[$index];
//            $obj[$property['property']] = Redis::get($key . '_' . $id);
            if (($property['type']) === 'int') {
                $obj[$property['property']] = (int) $obj[$property['property']];
            }
            $index++;
        }
        $obj['id'] = (int) $id;

        return (new RoomEntity($obj));
    }

    public static function getFindQuery($id)
    {
        $arrayMapping = self::MAPPING_PROPERTY;

        $mgetKeys = [];
        foreach ($arrayMapping as $key => $property) {
            $mgetKeys[] = $key . '_' . $id;
        }
        return $mgetKeys;
    }

    /**
     * @param RoomEntity $room
     *
     * @return \Laka\Lib\Support\Collection|null
     */
    public static function getRoomMemberByRoom(RoomEntity $room): ?\Laka\Lib\Support\Collection {
        $listMemberRoom = Redis::SMEMBERS('ROOMMEMBER_' . $room->id);

        $queries = [];
        foreach ($listMemberRoom as $id) {
            $queries[] = RoomMemberRepositoryOptimize::getFindQuery($room->id, (int) $id);
        }
        $values = Redis::pipeline(function ($pipe) use ($queries) {
            foreach ($queries as $query) {
                $pipe->mget($query);
            }
        });

//        $values = array_chunk($values, count(RoomMemberRepositoryOptimize::MAPPING_PROPERTY));

        $rs = [];
        foreach ($listMemberRoom as $index => $id) {
            $rs[] = RoomMemberRepositoryOptimize::find($room->id, (int) $id, $values[$index]);
        }

        return LakaCollect($rs);
    }

    /**
     * @param RoomMemberEntity $roomMemberEntity
     *
     * @return bool
     */
    public static function UserExistsOnRoom(RoomMemberEntity $roomMemberEntity): bool {
        return Redis::exists('ROOMMEMBER_ID_' . $roomMemberEntity->getRoomId() . '_' . $roomMemberEntity->getUserId());
    }

    /**
     * Lấy
     * @param $user_id
     * @param $contact_id
     *
     * @return integer|null
     */
    public static function getTwinRoomByTwoUserId($user_id, $contact_id): ?RoomEntity {

        $listRoomId1 = Redis::SMEMBERS('ROOM_BYUSERID_' . $user_id);
        $listRoomId2 = Redis::SMEMBERS('ROOM_BYUSERID_' . $contact_id);
        $tmp = [];
        foreach ($listRoomId1 as $v1) {
            foreach ($listRoomId2 as $v2) {
                if ($v1 === $v2) {
                    $tmp[] = $v1;
                }
            }
        }
        foreach ($tmp as $v) {
            $entity = RoomRepositoryOptimize::find((int) $v);
            if ($entity->name === "Twins Room") {
                return $entity;
            }
        }

        return null;
    }
}
