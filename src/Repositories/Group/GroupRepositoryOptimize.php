<?php

namespace Laka\Lib\Repositories\Group;

use App\User;
use Faker\Factory;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Laka\Lib\Entities\BaseEntity;
use Laka\Lib\Entities\GroupEntity;
use Laka\Lib\Entities\GroupItemEntity;
use Laka\Lib\Factories\GroupFactory;
use Laka\Lib\Factories\GroupItemFactory;
use Laka\Lib\Repositories\Room\RoomRepositoryOptimize;

class GroupRepositoryOptimize {

    const entity_name      = 'group';
    const MAPPING_PROPERTY = [
        'GROUP_NAME'    => ['property'=>'name','type'=>'string'],
        'GROUP_CREATED' => ['property'=>'created_at','type'=>'string'],
        'GROUP_UPDATED' => ['property'=>'updated_at','type'=>'string'],
        'GROUP_STATUS'  => ['property'=>'status','type'=>'string'],
    ];

    public static function allByUser(User $user): ?\Laka\Lib\Support\Collection {
        $rs = Cache::remember('allGroup' . $user->id, 10000000, function () use ($user) {
            $listGroupId = Redis::SMEMBERS('GROUP_LIST_' . $user->id);
            $rs          = [];
            foreach ($listGroupId as $groupId) {
                $rs[] = GroupRepositoryOptimize::find($user->id, $groupId);
            }

            return LakaCollect($rs);
        });

        return $rs;
    }

    public static function all(): ?\Laka\Lib\Support\Collection {
        $return = [];

        return LakaCollect($return);
    }

    public static function find(int $useId, int $groupId): ?GroupEntity {
        $arrayMapping = [
            'GROUP_NAME'    => ['property' => 'name', 'type' => 'string'],
            'GROUP_CREATED' => ['property' => 'created', 'type' => 'string'],
            'GROUP_UPDATED' => ['property' => 'updated', 'type' => 'string'],
            'GROUP_STATUS'  => ['property' => 'status', 'type' => 'int'],
        ];

        $obj = [];
        foreach ($arrayMapping as $key => $property) {

            $obj[$property['property']] = Redis::get($key . '_' . $useId . '_' . $groupId);
            if (($property['type']) === 'int') {
                $obj[$property['property']] = (int) $obj[$property['property']];
            }
        }
        $obj['id'] = (int) $groupId;
        $obj['user_id'] = (int) $useId;

        return (new GroupEntity($obj));
    }

}
