<?php

namespace Laka\Lib\Repositories\Group;

use App\User;
use Faker\Factory;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Laka\Lib\Entities\BaseEntity;
use Laka\Lib\Entities\GroupEntity;
use Laka\Lib\Entities\GroupItemEntity;
use Laka\Lib\Factories\GroupFactory;
use Laka\Lib\Factories\GroupItemFactory;

class GroupItemRepositoryOptimize {

    const entity_name      = 'group_item';
    const MAPPING_PROPERTY = [
        'GROUPITEM_CREATED' => ['property' => 'created', 'type' => 'string'],
        'GROUPITEM_UPDATED' => ['property' => 'updated', 'type' => 'string'],
        'GROUPITEM_STATUS'  => ['property' => '', 'type' => 'string'],
    ];

    public static function allByGroup(GroupEntity $group_entity): ?\Laka\Lib\Support\Collection {

        $rs = Cache::rememberForever('allGroupItem' . $group_entity->id,  function () use ($group_entity) {
            $listRoomId = Redis::SMEMBERS('GROUPITEM_LIST_' . $group_entity->id);

            $rs = [];
            foreach ($listRoomId as $roomId) {
                $rs[] = GroupItemRepositoryOptimize::find($group_entity->id, (int) $roomId);
            }

            return LakaCollect($rs);
        });

        return $rs;
    }

    public static function all(): ?\Laka\Lib\Support\Collection {
        return LakaCollect([]);

    }

    public static function find(int $groupId, int $roomId): ?GroupItemEntity {
        $arrayMapping = [
            'GROUPITEM_CREATED' => ['property' => 'created', 'type' => 'string'],
            'GROUPITEM_UPDATED' => ['property' => 'updated', 'type' => 'string'],
            'GROUPITEM_STATUS'  => ['property' => 'status', 'type' => 'int'],
        ];

        $obj = [];
        foreach ($arrayMapping as $key => $property) {

            $obj[$property['property']] = Redis::get($key . '_' . $groupId . '_' . $roomId);
            if (($property['type']) === 'int') {
                $obj[$property['property']] = (int) $obj[$property['property']];
            }
        }
        $obj['group_id'] = (int) $groupId;
        $obj['room_id']  = (int) $roomId;

        return (new GroupItemEntity($obj));
    }


}
